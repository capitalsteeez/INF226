# README: On Compulsory Assignment 2 #



##  Make a note of what's wrong with the structure (and anything else you see that may be problematic) – could any of these things have a security impact?:

Yes, it would make the code more maintainable and easy to navigate, which in terms could make the code more secure. 


## Features and documentation of my choices in:
1. Experiencing reportMissingModelSource problems and reportMissingImports. Therefore had to manually write the code here on GitLab, and it is no longer possible to run in flask and therefore neither ZAP. 
2. Imported os and implemented a randomly generated 24 letter secret key. 
3. Updated cookie settings to more easily log active user ID's. 


## Implement a simple instant messaging system, for example, a message has: ##

* ID implemented 

## You need a simple messaging API: ## 
Used existing messaging

## Authentication - Implement login ##
Used existing login

## Authorization - each user should only be able to see his/her own messages ## 


# Part 2B - Documentation # 

## How to demo the code:
1. Clone this repository 
2. Open terminal
3. Navigate to flask and start a session
4. Navigate to folder of repository
5. flask --app app run
6. Run OWASP Zap and have fun with SQL Injection


# **Questions**
## Threat model – who might attack the application? What can an attacker do? What damage could be done (in terms of confidentiality, integrity, availability)? Are there limits to what an attacker can do? Are there limits to what we can sensibly protect against?

The server is definetly vulnerable to SQL Injection and Cross-site request forgery. 


##  What are the main attack vectors for the application?

	* SQL Injection:
		https://owasp.org/www-community/attacks/SQL_Injection


	* Cross-site request forgery:
		https://owasp.org/www-community/attacks/csrf


	* Broken Access Control:
		https://owasp.org/Top10/A01_2021-Broken_Access_Control/

	* Injection:
		https://owasp.org/Top10/A03_2021-Injection/

	* Insecure Design: 
		https://owasp.org/Top10/A04_2021-Insecure_Design/

	* Security Misconfiguration:
		https://owasp.org/Top10/A05_2021-Security_Misconfiguration/

	* Identification and Authentication Failures:
		https://owasp.org/Top10/A07_2021-Identification_and_Authentication_Failures/

	* Software and Data Integrity Failures: 
		https://owasp.org/Top10/A08_2021-Software_and_Data_Integrity_Failures/

## What should we do (or what have you done) to protect against attacks?

What I have done: 
1. Implemented a randomly generated 24 letter secret key.
2. Updated cookie settings to more easily log active user ID's.This will send the cookies over SSL encrypted wire rather than plain text. If an attacker on the same network tries to sniff the network, they will never get the plain text cookies and so they can never be stolen with XSS.

What I should have done in addition:
1. Taken advantage of flask_wtf.csrf import CSRFProtect to enable Cross-Site Request Forgery protection.
2. Made the project in a better format (overview) to maintain the code more easily. 
3. Implementation of proper HTTP Headers. 

## What is the access control model?

The access control model enables you to control the ability of a process to access securable objects or to perform varius system administration tasks. 
There are two parts of the access control model: 
	* Acccess tokens: which contain information about a logged-on user
	* Security descriptors, which contain the security information that protects a securable object. 

	When a user logs on the system authenticates the users account name and password. If the logon is successful, the system creates an access token. Every process executed on behalf of this user will have a copy of this access token. 
	Every process executed on behalf of this user will have a copy of this access token. The access token contains security identifiers that identify the users account and any group accounts to which the user belongs. The token also contains a list of the privileges held by the user. A security descriptor identifies the objects owner and can also contain the following access control lists: 
		* A discretionary access control list: that identifies the users and groups allower or denied access to the object
		* A system access control list: that controls how the system audits attempts to access the object. 

Source: OWASP

## How can you know that you security is good enough? (traceability)

By focusing on building application logging mechanism, especially related to security logging. 
Many systems enable network device, OS, web server, mail server and database server logging, but often custom application even logging is missing, disabled or poorly configured. It provides much greater insight than ifrastructure logging alone. 

Application logging should be consistent within the application, consistent accros an organizations application portfolio and use industry standards where relevant, so the logged event data can be consumed, correlated, analyzed and magaed by a wide variety of systems. 

Application logs are invalueble for:
	* Identifying security incidents
	* Monitoring policy violations
	* Establishing baselines
	* Assisting non-repudiation controls (note that the trait non-repudiation is hard to achieve for logs because their trustworthiness is often just based on the logging party being audited properly while mechanisms like digital signatures are hard to utilize here)
	* Providing information about problems and unusual conditions
	* Contributing additional application-specific data for incident investigation which is lacking in other log sources
	* Helping defend against vulnerability identification and exploitation through attack detection

Application logging might also be used to record other types of events too such as:
	* Security events
	* Compliance monitoring
	* Audit trails e.g. data addition, modification and deletion, data exports

Design, implementation and testing: 

The application itself has access to a wide range of information events that should be used to generate log entries including info on users (identity, roles, permissions etc).
	* Client software e.g. actions on desktop software and mobile devices in local logs or using messaging technologies, JavaScript exception handler via Ajax, web browser such as using Content Security Policy (CSP) reporting mechanism
	* Embedded instrumentation code
	* Application firewalls e.g. filters, guards, XML gateways, database firewalls, web application firewalls (WAFs)
	* Closely-related applications e.g. filters built into web server software, web server URL redirects/rewrites to scripted custom error pages and handlers
	* Database applications e.g. automatic audit trails, trigger-based actions and more. 


Source (and complete information) see: https://cheatsheetseries.owasp.org/cheatsheets/Logging_Cheat_Sheet.html


## Same-origin polcy ##

The same-origin polcy is a critical security mechanism that restricts how a document or script loaded by one origin can interact with a resource from another origin. It helps isolate potentially malicious documents, reducing possible attack vectors. For example, it prevents a malicious website on the Internet from running JS in a browser to read data from a third-party webmail service (which the user is signed into) or a company intranet (which is protected from direct access by the attacker by not having a public IP address) and relaying that data to the attacker. 

Scrips executed from pages with an about:blanc or javascript: URL inherit the origin of the document containin that URL, since these types of URLs do not contain information about an origin server. 

*Source*: https://developer.mozilla.org/en-US/docs/Web/Security/Same-origin_policy


## Cross-Origin Resource Sharing ## 

This is an HTTP-header based mechanism that allows a server to indicate any origins (domain, scheme, or port) other than its own from which a browser should permit loading resources. Cross-Origin Resource Sharing also relies on a mechanism by which browsers make a "preflight" request to the server hosting the cross-origin resource, in order to check that the server will permit the actual request. In that preflight, the browser sends headers that indicate the HTTP method and headers that will be user in the actual request. 
